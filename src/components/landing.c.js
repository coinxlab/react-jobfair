import React from 'react';

const LandingOld = () => {
    return (
        <div>
            <div className="clearfix"></div>
            <div className="single-page-header" >
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 col-sm-12 col-xs-12">
                            <div className="single-page-header-inner">
                                <div className="left-side">
                                    <div className="header-image">
                                        <img src="https://res.cloudinary.com/brainethic/image/upload/v1549189786/mybuddyNewLogo_rqzfwu.png" alt="" />
                                    </div>
                                    <div className="header-details">
                                        <h3>Job Fair Registrations received <span>Pride Circle ltd</span></h3>

                                    </div>
                                </div>
                                <div className="right-side">

                                    <nav id="breadcrumbs" className="white">
                                        <ul>
                                            {/* <li><a href="#">Home</a></li>
                                            <li><a href="#">Browse Companies</a></li>
                                             */}
                                            <li onClick={(e) => {
                                                if (window.confirm("Are you sure to logout? All unsaved changes will be removed now")) {
                                                    Object.keys(localStorage).forEach(k => localStorage.removeItem(k))
                                                    window.location.reload()
                                                }
                                            }}>Logout</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="background-image-container" style={{ backgroundImage: "url('http://www.vasterad.com/themes/hireo/images/single-company.jpg')" }}>
                </div>
            </div >

        </div>)
}
const HeaderBar = () => {
    return (
        <div id="header">
            <div className="container">
                <div id="logo">
                    <a href="index.html">
                        <img style={{ height: "100px" }} src="https://res.cloudinary.com/brainethic/image/upload/v1549189786/mybuddyNewLogo_rqzfwu.png" data-sticky-logo="https://res.cloudinary.com/brainethic/image/upload/v1549189786/mybuddyNewLogo_rqzfwu.png" data-transparent-logo="images/logo2.png" alt="" />
                    </a>
                </div>
                <div className="right-side">


                    <div className="header-widget">

                        <div className="header-notifications user-menu">



                            <div className="header-notifications-dropdown">


                                <div className="user-status">


                                    <div className="user-details">
                                        <div className="user-avatar status-online">
                                            <img src="images/user-avatar-small-01.jpg" alt="" />
                                        </div>
                                        <div className="user-name">
                                            Tom Smith <span>Freelancer</span>
                                        </div>
                                    </div>


                                </div>

                                <ul className="user-menu-small-nav">
                                    <li><a href="index-logged-out.html"><i className="icon-material-outline-power-settings-new"></i> Logout</a></li>
                                </ul>

                            </div>
                        </div>

                    </div>
                    <span className="mmenu-trigger">
                        <button className="hamburger hamburger--collapse" type="button">
                            <span className="hamburger-box">
                                <span className="hamburger-inner"></span>
                            </span>
                        </button>
                    </span>

                </div>

            </div>
        </div>
    )
}

const Landing = () => {
    let userInfo = JSON.parse(localStorage.getItem("user")) || {}
    let personName = "there"
    if (userInfo && userInfo._profile && userInfo._profile.name) {
        personName = userInfo._profile.name
    }
    return (

        <div>
            <div className="photo-section" style={{ marginBottom: "20px", backgroundImage: "url('http://www.vasterad.com/themes/hireo/images/section-background.jpg')" }}>


                <div className="text-content white-font">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8">
                            </div>

                            <div className="col-md-4">

                                <p className="signout">
                                    Not {personName} ? <a href="#" onClick={(e) => {
                                        if (window.confirm("Are you sure to logout? All unsaved changes will be removed now")) {
                                            Object.keys(localStorage).forEach(k => localStorage.removeItem(k))
                                            window.location.reload()
                                        }
                                    }}>Sign Out</a>
                                </p>
                            </div>
                        </div>
                        <div className="row">

                            <div className="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                                <h2>Hello {personName}, take next step for your job hunt</h2>
                                {/*  <p>
                                    We love to contribute to your success and finding the dream job in this job fair. Enroll here to attend of the event
                                </p> */}

                                {/* <a href="pages-pricing-plans.html" className="button button-sliding-icon ripple-effect big margin-top-20" style="width: 154.875px;">
                    Get Started <i className="icon-material-outline-arrow-right-alt"></i>
                </a> */}
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div className="after">
            </div>

        </div>
    )
}
export { Landing, HeaderBar, LandingOld }