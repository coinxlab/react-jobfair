import React, { Component } from 'react'
import { FileUpload } from './FileUpload.c';
import { submitProfile, updateUserInfo, getConfig, getUserInfo } from './data.service'
import { config } from './config.data'
import { TabComposer } from './builder/TabComposer.c';

export class StudentInfo extends Component {

  constructor() {
    super();

    let c = JSON.parse(localStorage.getItem("config") || null)
    if (c) {
      Object.assign(config, c)
    }
    console.log(config, c)
    this.state = {
      activeTab: 1,
      composer: config,
      isAccepted: false
    };
    this.activateTab = this.activateTab.bind(this);
    this.changeState = this.changeState.bind(this)
  }

  changeState() {

    if (this.state.isAccepted) {
      this.setState({ isAccepted: false })
    } else {
      this.setState({ isAccepted: true })
    }
  }

  resetThenSet = (id, key) => {
    let temp = JSON.parse(JSON.stringify(this.state[key]));
    temp.map(item => item.id === id ? item.selected = true : item.selected = false);
    this.setState({
      [key]: temp
    });
  }

  nextTab(n) {
    if (n < 5)
      this.setState({
        activeTab: n
      })
    else {
      if (!this.state.isAccepted) {
        alert("Please accept terms and conditions")
        return;
      }
      this.submitUserProfile()
    }
  }
  activateTab = (e, n) => {
    e.preventDefault();

    this.setState({ activeTab: n })
  }

  submitUserProfile() {
    submitProfile((cb) => {

      if (cb.id) {
        updateUserInfo(cb)
        alert(`Thank you for submitting/updating your profile!`)
      } else {
        alert(`There is some problem while submitting your profile, please try again later`)
      }
    })
  }

  getResumeFile() {
    console.log(getUserInfo()["resumeFile"])
    return getUserInfo()["resumeFile"].fileDownloadUri
  }
  render() {

    return (
      <div className="row">
        <div className="box-overlay">

          <div className="container" >
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="tabs">
                  <div className="tabs-header">
                    <ul>
                      <li><a className={(this.state.activeTab === 1 ? 'active-tabhead' : '')} href="javascript:void();" onClick={(e) => this.activateTab(e, 1)}>Personal Information</a></li>
                      <li><a className={(this.state.activeTab === 2 ? 'active-tabhead' : '')} href="javascript:void();" onClick={(e) => this.activateTab(e, 2)}>Education</a></li>
                      <li><a className={(this.state.activeTab === 3 ? 'active-tabhead' : '')} href="javascript:void();" onClick={(e) => this.activateTab(e, 3)}>Employement</a></li>
                      <li><a className={(this.state.activeTab === 4 ? 'active-tabhead' : '')} href="javascript:void();" onClick={(e) => this.activateTab(e, 4)}>Resume Upload</a></li>
                    </ul>
                  </div>

                  <div className="tabs-content" style={{ paddingBottom: '100px' }}>
                    <div className={(this.state.activeTab === 1 ? 'tab active' : 'tab')} data-tab-id="1">
                      <TabComposer tabContent={config.personal} tabType="personal" />
                      <input type="submit" onClick={(e) => this.nextTab(2)} className="submit button margin-top-15" value="Save" style={{ marginLeft: "925px" }}></input>
                    </div>

                    <div className={(this.state.activeTab === 2 ? 'tab active' : 'tab')} data-tab-id="2">
                      <TabComposer tabContent={config.education} tabType="education" />
                      <input type="submit" onClick={(e) => this.nextTab(3)} className="submit button margin-top-15" value="Save" style={{ marginLeft: "925px" }}></input>
                    </div>

                    <div className={(this.state.activeTab === 3 ? 'tab active' : 'tab')} data-tab-id="3">
                      <TabComposer tabContent={config.employment} tabType="employment" />
                      <input type="submit" onClick={(e) => this.nextTab(4)} className="submit button margin-top-15" value="Save" style={{ marginLeft: "925px" }}></input>
                    </div>

                    <div className={(this.state.activeTab === 4 ? 'tab active' : 'tab')} data-tab-id="4">
                      <h3>Attach your resume</h3>
                      <div className="">
                        You have attached <a href={this.getResumeFile()} target="__blank">this resume</a>
                      </div>
                      <FileUpload></FileUpload>
                      <div className="row margin-top-25">
                        <div class="checkbox">
                          <input onClick={this.changeState} type="checkbox" id="chekcbox1"></input>
                          <label for="chekcbox1"><span class="checkbox-icon"></span>
                            I accept the terms and conditions &nbsp;
                  <a href="https://resume.thepridecircle.com/resume_tnc.docx" target="__blank">Read more </a>
                          </label>

                        </div>
                      </div>
                      <input type="submit" onClick={(e) => this.nextTab(5)}
                        className="button ripple-effect big margin-top-30"
                        value="Submit your profile"
                        style={{ marginLeft: "800px" }}></input>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div >
      </div>
    )
  }

}
