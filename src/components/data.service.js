import axios from 'axios'

const serverURL = `https://resume.thepridecircle.com:8099`

const handleUpload = (endpoint, selectedFile, cb) => {
  const data = new FormData()
  data.append('file', selectedFile, selectedFile.name)

  axios
    .post(endpoint, data, {
      onUploadProgress: ProgressEvent => {
        console.log(ProgressEvent)
      },
    })
    .then(res => {
      console.log(res)
      cb(res.data)
    })
}

const getRequest = (endpoint, cb) => {
  axios.get(endpoint)
    .then(function (response) {
      console.log(response);
      cb(response.data)
    })
    .catch(function (error) {
      cb(error)
      console.log(error);
    });
}

const putRequest = (endpoint, query, payload) => {

}

const postRequest = (endpoint, payload, cb) => {
  axios.post(endpoint, payload)
    .then(function (response) {
      console.log(response);
      cb(response.data)
    })
    .catch(function (error) {
      cb(error)
      console.log(error);
    });
}

const deleteRequest = (endpoint, query, payload) => {

}

const submitProfile = (cb) => {
  let userProfile = getUserInfo()

  if (userProfile && userProfile.personal && userProfile.personal.lastName) {
    userProfile.personal.lastname = userProfile.personal.lastName
  }

  if (userProfile && userProfile.employment && userProfile.employment.skills) {
    if (typeof userProfile.employment.skills == 'string')
      userProfile.skills = (userProfile.employment.skills || "").split(",")
    else
      userProfile.skills = userProfile.employment.skills
  }

  if (userProfile && userProfile.employment && userProfile.employment.prefWorkLocations) {
    if (typeof userProfile.employment.prefWorkLocations == 'string')
      userProfile.employment.prefWorkLocations = (userProfile.employment.prefWorkLocations || "").split(",")
  }

  if (userProfile && userProfile.personal) {
    if (userProfile.personal.phone) {

      if (/^\d{10}$/.test(userProfile.personal.phone)) {
        // value is ok, use it
      } else {
        alert("Invalid mobile number; must be ten digits")
        return false
      }
    }
  }
  if (userProfile && userProfile.personal) {
    if (userProfile.personal.email) {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

      if (reg.test(userProfile.personal.email) == false) {
        alert('Invalid Email Address');
        return false;
      }

    }
  }


  userProfile.userId = getLoggedInUserProfile().id
  console.log("Submitting", userProfile)
  postRequest(`${serverURL}/resume-mgr/save-update`, userProfile, cb)
}

const getStudentProfiles = (query, cb) => {
  console.log("search with", query)
  postRequest(`${serverURL}/resume-mgr/generic-query`, query, cb)
}

const getConfig = (cb) => {
  getRequest("https://jsonblob.com/api/jsonBlob/cd5688e6-3737-11e9-adf7-9d3f01fc2305", cb)
}

const updateUserInfo = (newUserObject) => {
  let existing = getUserInfo()
  let newUserInfo = Object.assign(existing, newUserObject);
  localStorage.setItem("userInfo", JSON.stringify(newUserInfo || {}))
}

const getUserInfo = () => {
  return JSON.parse(localStorage.getItem("userInfo")) || {}
}

const getLoggedInUser = () => {
  return JSON.parse(localStorage.getItem("user")) || {}
}
const getLoggedInUserProfile = () => {
  let user = getLoggedInUser() || {}
  if (user && user._profile) {
    return user._profile
  }
  return null
}


export { getConfig, handleUpload, getUserInfo, updateUserInfo, submitProfile, getStudentProfiles, getLoggedInUserProfile }