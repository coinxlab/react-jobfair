import React, { Component } from 'react'
import { Row } from './Row.c';
export class TabComposer extends Component {

    constructor() {
        super();
        this.getRows = this.getRows.bind(this)
    }
    render() {

        return (
            <div>
                {this.getRows()}
            </div>
        )
    }
    shouldComponentUpdate() {
        return false;
    }
    getRows() {
        console.log("Tab composer", this.props)
        if (!this.props.tabContent || !this.props.tabContent.length) return
        let rowTemplate = []
        const list = (this.props.tabContent);
        const chunkSize = 2;
        let allRows = new Array(Math.ceil(list.length / chunkSize)).fill().map(_ => list.splice(0, chunkSize))
        allRows.forEach((e, i) => {
            rowTemplate.push(<Row key={"tc_" + this.props.tabType + "_" + i} components={e} tabType={this.props.tabType} />)
        });
        return rowTemplate;
    }
}