import React, { Component } from 'react'
import { updateUserInfo, getUserInfo } from './../data.service'
export class Input extends Component {

    existingValue = ""
    constructor() {
        super();
        this.getPlaceHolder = this.getPlaceHolder.bind(this)
        this.setValue = this.setValue.bind(this)
        this.state = {
            existingValue: ""
        }
    }
    render() {
        return (
            <div>
                <div style={{ textAlign: 'left' }} class="section-headline margin-top-5 margin-bottom-5">
                    <h6 style={{ fontSize: '14px' }}>{this.getPlaceHolder()}</h6>
                </div>
                <div className="input-with-icon-left">

                    <input className="with-border"
                        name={this.props.config.name}
                        type={this.props.config.type}
                        id={this.props.config.name}
                        placeholder={this.getPlaceHolder()}
                        value={this.state.existingValue}
                        onChange={this.setValue}
                        required="required" />
                    <i className={this.props.config.icon}></i>
                </div>
            </div >
        )
    }
    getPlaceHolder() {
        this.state = {
            existingValue: this.getTabData()[this.props.config.name || this.props.config]
        }
        if (this.props.config.placeholder) return this.props.config.placeholder
        return (this.props.config.name || "")
            .replace(/([a-z]+)*([A-Z][a-z])/g, "$1 $2").split(' ')
            .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
            .join(' ')
    }
    getTabData() {
        return getUserInfo()[this.props.tabType] || {}
    }
    setValue(e) {
        let k = this.props.config.name || this.props.config
        let o = {
            [this.props.tabType]: this.getTabData()
        }

        o[this.props.tabType][k] = e.target.value
        this.setState(o)
        console.log(k)
        updateUserInfo(o)
    }
}