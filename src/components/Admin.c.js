import React, { Component } from 'react';
import { config } from './config.data';
import { getStudentProfiles } from './data.service';
import { Dropdown } from 'reactjs-dropdown-component';


export class Admin extends Component {

  constructor() {
    super();
    console.log(config)
    let c = JSON.parse(localStorage.getItem("config") || null)
    if (c) {
      Object.assign(config, c)
    }

    this.getCSV = this.getCSV.bind(this)
    this.updateQuery = this.updateQuery.bind(this)
    this.searchWithQuery = this.searchWithQuery.bind(this)
    this.clearSearch = this.clearSearch.bind(this)
    this.state = {
      tableData: null,
      filter: {},
      queryMap: {
      },
      dd: {
        education_yearOfPassing: {
          title: "Select Batch",
          values: [{ "id": "1990", "title": "1990", "selected": false, "key": "education_yearOfPassing" }, { "id": "1991", "title": "1991", "selected": false, "key": "education_yearOfPassing" }, { "id": "1992", "title": "1992", "selected": false, "key": "education_yearOfPassing" }, { "id": "1993", "title": "1993", "selected": false, "key": "education_yearOfPassing" }, { "id": "1994", "title": "1994", "selected": false, "key": "education_yearOfPassing" }, { "id": "1995", "title": "1995", "selected": false, "key": "education_yearOfPassing" }, { "id": "1996", "title": "1996", "selected": false, "key": "education_yearOfPassing" }, { "id": "1997", "title": "1997", "selected": false, "key": "education_yearOfPassing" }, { "id": "1998", "title": "1998", "selected": false, "key": "education_yearOfPassing" }, { "id": "1999", "title": "1999", "selected": false, "key": "education_yearOfPassing" }, { "id": "2000", "title": "2000", "selected": false, "key": "education_yearOfPassing" }, { "id": "2001", "title": "2001", "selected": false, "key": "education_yearOfPassing" }, { "id": "2002", "title": "2002", "selected": false, "key": "education_yearOfPassing" }, { "id": "2003", "title": "2003", "selected": false, "key": "education_yearOfPassing" }, { "id": "2004", "title": "2004", "selected": false, "key": "education_yearOfPassing" }, { "id": "2005", "title": "2005", "selected": false, "key": "education_yearOfPassing" }, { "id": "2006", "title": "2006", "selected": false, "key": "education_yearOfPassing" }, { "id": "2007", "title": "2007", "selected": false, "key": "education_yearOfPassing" }, { "id": "2008", "title": "2008", "selected": false, "key": "education_yearOfPassing" }, { "id": "2009", "title": "2009", "selected": false, "key": "education_yearOfPassing" }, { "id": "2010", "title": "2010", "selected": false, "key": "education_yearOfPassing" }, { "id": "2011", "title": "2011", "selected": false, "key": "education_yearOfPassing" }, { "id": "2012", "title": "2012", "selected": false, "key": "education_yearOfPassing" }, { "id": "2013", "title": "2013", "selected": false, "key": "education_yearOfPassing" }, { "id": "2014", "title": "2014", "selected": false, "key": "education_yearOfPassing" }, { "id": "2015", "title": "2015", "selected": false, "key": "education_yearOfPassing" }, { "id": "2016", "title": "2016", "selected": false, "key": "education_yearOfPassing" }, { "id": "2017", "title": "2017", "selected": false, "key": "education_yearOfPassing" }, { "id": "2018", "title": "2018", "selected": false, "key": "education_yearOfPassing" }, { "id": "Other", "title": "Other", "selected": false, "key": "education_yearOfPassing" }]
        },
        education_course: {
          title: "Select course",
          values: [{ "id": "B.A.", "title": "B.A.", "selected": false, "key": "education_course" }, { "id": "B. Arch", "title": "B. Arch", "selected": false, "key": "education_course" }, { "id": "BCA", "title": "BCA", "selected": false, "key": "education_course" }, { "id": "B.B.A / B.M.S", "title": "B.B.A / B.M.S", "selected": false, "key": "education_course" }, { "id": "B.Com", "title": "B.Com", "selected": false, "key": "education_course" }, { "id": "B.Ed", "title": "Civil Engineering", "selected": false, "key": "education_course" }, { "id": "BDS", "title": "BDS", "selected": false, "key": "education_course" }, { "id": "BHM", "title": "BHM", "selected": false, "key": "education_course" }, { "id": "B. Pharma", "title": "B. Pharma", "selected": false, "key": "education_course" }, { "id": "B.Sc", "title": "Medical", "selected": false, "key": "education_course" }, { "id": "B. Tech / B.E.", "title": "B. Tech / B.E.", "selected": false, "key": "education_course" }, { "id": "LLB", "title": "LLB", "selected": false, "key": "education_course" }, { "id": "M.A", "title": "M.A", "selected": false, "key": "education_course" }, { "id": "MCA", "title": "MCA", "selected": false, "key": "education_course" }, { "id": "MBA", "title": "MBA", "selected": false, "key": "education_course" }, { "id": "M. Com", "title": "M. Com", "selected": false, "key": "education_course" }, { "id": "M. Pharma", "title": "M. Pharma", "selected": false, "key": "education_course" }, { "id": "M. Sc", "title": "M. Sc", "selected": false, "key": "education_course" }, { "id": "M. Tech / M.E.", "title": "M. Tech / M.E.", "selected": false, "key": "education_course" }, { "id": "Other", "title": "Other", "selected": false, "key": "education_course" }]
        },
        employment_functionalArea: {
          "title": "Select ",
          "values": [{ "id": "Accounts", "title": "Accounts / Finance / TAX / Company Secretary / Audit", "selected": false, "key": "employment_functionalArea" }, { "id": "Analytics", "title": "Analytics & Data Mining", "selected": false, "key": "employment_functionalArea" }, { "id": "Architecture", "title": "Architecture", "selected": false, "key": "employment_functionalArea" }, { "id": "Services", "title": "Beauty / Fitness / Spa Services", "selected": false, "key": "employment_functionalArea" }, { "id": "CSR&Sustainability", "title": "CSR & Sustainability", "selected": false, "key": "employment_functionalArea" }, { "id": "DefenceServices", "title": "Defence / Security Services", "selected": false, "key": "employment_functionalArea" }, { "id": "Design", "title": "Design / Creative / UI / UX", "selected": false, "key": "employment_functionalArea" }, { "id": "Engineering", "title": "Engineering / R&D", "selected": false, "key": "employment_functionalArea" }, { "id": "Data Entry", "title": "Executive Assistant / Front Office / Data Entry", "selected": false, "key": "employment_functionalArea" }, { "id": "FashionDesigning", "title": "Fashion Designing / Merchandising", "selected": false, "key": "employment_functionalArea" }, { "id": "Financial Services", "title": "Financial Services / Insurance / Banking", "selected": false, "key": "employment_functionalArea" }, { "id": "Hotel", "title": "Hotel / Restaurant", "selected": false, "key": "employment_functionalArea" }, { "id": "HR", "title": "HR / Recruitment / Admin", "selected": false, "key": "employment_functionalArea" }, { "id": "ITHardware", "title": "IT- Hardware", "selected": false, "key": "employment_functionalArea" }, { "id": "ITApplicationbuilding", "title": "IT - Application building", "selected": false, "key": "employment_functionalArea" }, { "id": "ITClient", "title": "IT Client / Server", "selected": false, "key": "employment_functionalArea" }, { "id": "ITDBA", "title": "IT DBA /Data Warehousing", "selected": false, "key": "employment_functionalArea" }, { "id": "ITEComm", "title": "IT - E-Commerce", "selected": false, "key": "employment_functionalArea" }, { "id": "ITEmbed", "title": "IT - Embedded / VLSI / EDA", "selected": false, "key": "employment_functionalArea" }, { "id": "ITERM", "title": "IT - ERM / ERP / CRM", "selected": false, "key": "employment_functionalArea" }, { "id": "ITMainframe", "title": "IT - Mainframe", "selected": false, "key": "employment_functionalArea" }, { "id": "ITMobile", "title": "IT- Mobile", "selected": false, "key": "employment_functionalArea" }, { "id": "ITNetworkAdmin", "title": "IT -Network Admin / Security", "selected": false, "key": "employment_functionalArea" }, { "id": "ITTelecom", "title": "IT -Telecom", "selected": false, "key": "employment_functionalArea" }, { "id": "CustomerService", "title": "ITES /BPO /KPO /LPO /Customer Service", "selected": false, "key": "employment_functionalArea" }, { "id": "Legal", "title": "Legal / IP", "selected": false, "key": "employment_functionalArea" }, { "id": "Marketing", "title": "Marketing /PR / Media", "selected": false, "key": "employment_functionalArea" }, { "id": "Manufacturing", "title": "Manufacturing /Production", "selected": false, "key": "employment_functionalArea" }, { "id": "Sales", "title": "Sales /Retail", "selected": false, "key": "employment_functionalArea" }, { "id": "ProjectManagement", "title": "Project Management", "selected": false, "key": "employment_functionalArea" }, { "id": "Logistics", "title": "Logistics / Supply Chain", "selected": false, "key": "employment_functionalArea" }, { "id": "Travel", "title": "Travel / Tour / Airline", "selected": false, "key": "employment_functionalArea" }, { "id": "Other", "title": "Other", "selected": false, "key": "employment_functionalArea" }]
        }
      }
    }
  }
  resetThenSet = (id, key) => {
    let temp = JSON.parse(JSON.stringify(this.state.dd[key]["values"]));
    temp.map(item => item.id === id ? item.selected = true : item.selected = false);
    let o = {
      queryMap: this.state.queryMap
    }
    o.queryMap[key.replace("_", ".")] = id
    this.setState(o);
  }

  render() {
    this.getColSpans();
    return (
      <div>

        <div >
          {this.getSearchComponent()}
        </div>
      </div>

    )
  }
  getSearchComponent() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xl-3 col-lg-4">
            <div className="sidebar-container">
              <div className="sidebar-widget">
                <h4>Preferred locations</h4>
                <div className="input-with-icon">
                  <div id="autocomplete-container">
                    <input value={this.state.queryMap.employment_prefWorkLocations} id="employment_prefWorkLocations" onChange={this.updateQuery} type="text" placeholder="Seperate with comma (,)" autoComplete="off" />
                  </div>

                </div>
              </div>
              <div className="sidebar-widget">
                <h4>Skills</h4>
                <div className="input-with-icon">
                  <div id="autocomplete-container">
                    <input value={this.state.queryMap.skills} id="skills" onChange={this.updateQuery} type="text" placeholder="Seperate with comma (,)" autoComplete="off" />
                  </div>

                </div>
              </div>

              <div className="sidebar-widget">
                <h4>Experience</h4>
                <div className="input-with-icon">
                  <div id="autocomplete-container">
                    <input value={this.state.queryMap.employment_totalExperience} id="employment_totalExperience" onChange={this.updateQuery} type="text" placeholder="enter experience" autoComplete="off" />
                  </div>

                </div>
              </div>
              <div className="sidebar-widget">
                <h4>Need accomodation</h4>
                <div className="input-with-icon">
                  <div id="autocomplete-container">
                    <input value={this.state.queryMap.employment_accomodationRequired} id="employment_accomodationRequired" onChange={this.updateQuery} type="text" placeholder="true or false" autoComplete="off" />
                  </div>

                </div>
              </div>

              <div className="sidebar-widget">
                <h4>Custom search</h4>
                <div className="input-with-icon">
                  <div id="autocomplete-container">
                    <input value={this.state.queryMap.filter} id="filter" onChange={this.updateQuery} type="text" placeholder="field1:value1, field2:value2" autoComplete="off" />
                  </div>

                </div>
              </div>
              <div className="sidebar-widget">
                <h4>Batch</h4>
                <div className="btn-group bootstrap-select show-tick">

                  <Dropdown
                    title={this.state.dd.education_yearOfPassing.title}
                    list={this.state.dd.education_yearOfPassing.values}
                    resetThenSet={this.resetThenSet}
                  />
                </div>
              </div>
              <div className="sidebar-widget">
                <h4>Functional Area</h4>
                <div className="btn-group bootstrap-select show-tick">

                  <Dropdown
                    title={this.state.dd.employment_functionalArea.title}
                    list={this.state.dd.employment_functionalArea.values}
                    resetThenSet={this.resetThenSet}
                  />
                </div>
              </div>

              <div className="sidebar-widget">
                <h4>Course</h4>
                <div className="btn-group bootstrap-select show-tick">

                  <Dropdown
                    title={this.state.dd.education_course.title}
                    list={this.state.dd.education_course.values}
                    resetThenSet={this.resetThenSet}
                  />
                </div>
              </div>
              <div className="sidebar-widget">

                <div className="row">

                  <a onClick={this.searchWithQuery} href="#" className="button ripple-effect margin-top-10">
                    <i className="icon-material-outline-supervisor-account"></i>
                    Search

                  </a>

                  <a onClick={this.clearSearch} href="#" className="button ripple-effect margin-left-10 margin-top-10">

                    Clear

                  </a>
                  <a onClick={this.getCSV} href="#" className="button ripple-effect margin-top-10">
                    <i className="icon-material-outline-supervisor-account"></i>
                    Download CSV

                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-9 col-lg-8 content-left-offset">
            <div className="dashboard-box margin-top-0">
              <div className="headline">
                <h3><i className="icon-material-outline-assignment"></i> Student forms submitted</h3>
              </div>

              <div className="content">
                <ul className="dashboard-box-list">
                  {this.getListItem()}
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }
  updateQuery(e) {
    console.log(e.target.id, e.target.value)
    let o1 = e.target.id.replace("_", ".")
    let o = {}
    if (e.target.id == "filter") {
      try {

        o = JSON.parse(`{ ${e.target.value} }`)
      } catch (e) {
        console.warn("Invalid json structure for custom search just key and values will do")
      }
    }
    else {
      o[o1] = e.target.value
    }
    if (e.target.value == "") {
      delete this.state.queryMap[o1]
    } else {
      Object.assign(this.state.queryMap, o)
    }
  }
  componentDidMount() {
    this.searchWithQuery()
  }
  getListItem() {
    let l = []
    if (this.state && this.state.stud && this.state.stud.length)
      this.state.stud.forEach((e, i) => {
        l.push(
          <li key={"li_" + i}>
            <div className="job-listing width-adjustment">

              <div className="job-listing-details">

                <div className="job-listing-description">
                  <h3 className="job-listing-title"><a href="#">{e && e.personal && e.personal.preferedName} {e && e.personal && e.personal.legalName}</a>
                    <span className="dashboard-status-button yellow">{e && e.personal && e.personal.email}</span>
                    <span className="dashboard-status-button green">{e && e.personal && e.personal.phone}</span>
                    <a target="_blank" href={e && e.personal && e.personal.linkedlnUrl}>
                      <span className="dashboard-status-button yellow" >Linked In</span>
                    </a>
                    <a target="_blank" href={e && e.personal && e.personal.twitterUrl}>
                      <span className="dashboard-status-button green" >Twitter</span>
                    </a>

                    <a target="_blank" href={e.resumeFile && e.resumeFile.fileDownloadUri}>
                      <span className="dashboard-status-button green" >Resume</span>
                    </a>
                  </h3>

                  <div className="job-listing-footer">

                    <ul>
                      <li>Studied at {e.education && e.education.college} in <b> {e.education && e.education.course} - {e.education && e.education.specialization}</b>. Passout of {e.education && e.education.yearOfPassing} </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <ul className="dashboard-task-info">
              <li><strong>{(e.education && e.education.marks) || 'NA'}</strong><span>% marks</span></li>
              <li><strong>{(e.education && e.education.courseType) || 'NA'}</strong><span>Course Type</span></li>
              <li><strong>{(e.education && e.education.highestQualification) || 'NA'}</strong><span>Highest Qualification</span></li>
              <li><strong>{(e.employment && e.employment.totalExperience) || 0}</strong><span>Total Experience</span></li>
              <li><strong>{(e.employment && e.employment.expectedCTC) || 'NA'}</strong><span>Expected CTC</span></li>
              <li><strong>{(e.employment && e.employment.functionalArea) || 'NA'}</strong><span>Functional Area</span></li>
            </ul>
          </li>)
      });
    return l
  }
  ds = []
  csvData = []
  getColSpans() {
    let arr = Object.keys(config).map((x, i) => { return { colSpan: config[x].length, name: x } })

    //let colors = ["#99A3A4", "#AAB7B8", "#616A6B", "#D0D3D4", "#FAD7A0"]
    //let colSpans = []
    //const cols = []
    arr.forEach((e, i) => {
      //colSpans.push(<th key={"colSpan_" + i} colSpan={e.colSpan} style={{ background: colors[i] }} scope="colgroup">{e.name}</th>)
      config[e.name].forEach((y, j) => {
        this.ds.push([e.name, y.name])
        //cols.push(<th key={"col_" + i + "_" + j} style={{ background: colors[i] }} scope="col">{y.name}</th>)
      });
    });
    var a = []; a.push(...config.personal.map(x => ["personal", x.name]))
    a.push(...config.education.map(x => ["education", x.name]))
    a.push(...config.employment.map(x => ["employment", x.name]))

    a.push(["resumeFile", "fileDownloadUri"])
    this.ds = a
    console.log(a)
    //return { colSpans: colSpans, cols: cols }
  }

  prepareRows(data) {
    let rows = []
    this.csvData = [this.ds.map(x => x.join("_"))]
    data.forEach((d, i) => {
      let cols = [];
      let csvRow = []
      this.ds.forEach((seq, j) => {
        let [fk, sk] = seq
        //console.log(fk, sk)
        if (sk == 'lastName' || sk == 'lastname')
          sk = 'lastname'
        if (d[fk]) {
          let v = d[fk][`${sk}`]
          v = (v == true || v == 'true') ? 'Yes' : v
          v = (v == false || v == 'false') ? 'No' : v
          if (typeof v == 'array') v = v.join(", ")
          csvRow.push(this.getTDType(v))
          cols.push(<td data-label={seq.join(".")} key={seq.join("_") + i + "_" + j}>{this.getTDType(v)}</td>)
        }
      });
      this.csvData.push(csvRow)
      rows.push(<tr key={'stud_tr_' + i}>{cols}</tr>)
    });
    this.setState({ tableData: rows })
    //console.log(this.csvData, this.ds)
    return rows;
  }
  clearSearch(e) {
    if (e)
      e.preventDefault();
    this.setState({
      queryMap: {}
    })
    this.searchWithQuery(e)
  }
  searchWithQuery(e) {
    if (e)
      e.preventDefault();

    getStudentProfiles({ "queryMap": this.state.queryMap }, (r) => {
      console.log(r)
      this.setState({
        stud: r
      })
      if (r && r.length)
        this.prepareRows(r)
    },
      e => {
        this.setState({
          stud: []
        })
      })
  }
  getCSV(e) {
    e.preventDefault()
    let csvContent = this.csvData.map(x => x.join("|")).join("\n")
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent));
    pom.setAttribute('download', `student_report_${new Date().getTime()}.csv`);
    pom.click();
  }

  getTDType(value) {
    return value
  }

}