import React, { Component } from 'react';
import { GoogleButton, FacebookButton } from './SocialButton.c'
import { getStudentProfiles, updateUserInfo, getLoggedInUserProfile, getConfig } from './data.service'
export class Login extends Component {

    constructor() {
        super();
        this.handleSocialLogin = this.handleSocialLogin.bind(this)
        this.handleSocialLoginFailure = this.handleSocialLoginFailure.bind(this)
        getConfig((config) => {
            localStorage.setItem("config", JSON.stringify(config))
        })
    }

    handleSocialLogin(user) {
        console.log(user)

        localStorage.setItem("user", JSON.stringify(user))
        if (user._profile && ['originscan.stage@gmail.com', 'pridecircle.hire@gmail.com', 'pridecircles@gmail.com'].filter(x => x == user._profile.email)[0]) {
            localStorage.setItem("adminToken", user._profile.email)
            window.location.reload()
        } else {
            localStorage.setItem("token", user._token.accessToken)
            getStudentProfiles({ queryMap: { userId: getLoggedInUserProfile().id } }, (c) => {
                if (c[0]) {
                    if (c[0].skills) {
                        c[0].employment["skills"] = c[0].skills
                    }
                    console.warn("Fetched user profile")
                    updateUserInfo(c[0])
                }
                else {
                    updateUserInfo({ userId: user._profile.email })
                }
                setTimeout(() => {
                    window.location.reload()
                }, 500);
            })
        }
    }

    handleSocialLoginFailure(err) {
        console.error(err)
    }

    render() {
        return (

            <div>
                <div className="intro-banner" data-background-image="http://www.vasterad.com/themes/hireo/images/home-background-02.jpg">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="banner-headline">
                                    <img src="https://res.cloudinary.com/brainethic/image/upload/v1550946678/imageedit_1_5893435683_umpvz6.png" style={{ maxWidth: "250px" }} />
                                    <h1>
                                        <strong>Register for Job Fair and surf ocean of opportunities</strong>
                                    </h1>
                                    <br />
                                    <h3>
                                        <small>You have to get started with your social account </small>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <div className="row">
                            <div className="col-xl-5" style={{ minHeight: "300px" }}>
                                <div className="login-register-page">

                                    <div style={{ marginTop: "20px" }} className="social-login-buttons">
                                        <GoogleButton
                                            provider='google'
                                            appId='843185905312-dj8j79aajbbhuq6gr2tall7a7um6afet.apps.googleusercontent.com'
                                            onLoginSuccess={this.handleSocialLogin}
                                            onLoginFailure={this.handleSocialLoginFailure}
                                        />
                                        {/* <FacebookButton
                                            provider='facebook'
                                            appId='1101236706721941'
                                            onLoginSuccess={this.handleSocialLogin}
                                            onLoginFailure={this.handleSocialLoginFailure}
                                        /> */}
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div className="background-image-container"
                        style={{ backgroundImage: "url('http://www.vasterad.com/themes/hireo/images/home-background-02.jpg')" }}></div>
                </div>
            </div>
        )
    }
}














