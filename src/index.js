import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import './css/style.css';
import './css/colors/blue.css';

import App from './App';
/* 

import "./js/jquery-3.3.1.min.js"
import "./js/jquery-migrate-3.0.0.min.js"
import "./js/mmenu.min.js"
import "./js/tippy.all.min.js"
import "./js/bootstrap-slider.min.js"
import "./js/bootstrap-select.min.js"
import "./js/snackbar.js"
import "./js/clipboard.min.js"
import "./js/counterup.min.js"
import "./js/magnific-popup.min.js"
import "./js/slick.min.js"
import "./js/custom.js" */



import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
