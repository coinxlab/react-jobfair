import React, { Component } from 'react';
import './App.css';
import { Landing, HeaderBar, LandingOld } from './components/landing.c'
import { StudentInfo } from './components/StudentInfo.c'
import { Login } from './components/Login.c';
import { Admin } from './components/Admin.c';

class App extends Component {


  constructor() {
    super()
    this.state = { isToShow: false }
  }
  render() {

    if (localStorage.getItem("token")) {

      return (
        <div className="App" style={{ visibility: (this.state.isToShow === true ? "visible" : "hidden") }}>
          <Landing />
          <StudentInfo />
        </div>
      );
    }
    else if (localStorage.getItem("adminToken")) {

      return (
        <div className="App" style={{ visibility: (this.state.isToShow === true ? "visible" : "hidden") }}>

          <LandingOld />
          <Admin />
        </div>
      );
    }

    else {
      return (
        <div style={{ visibility: (this.state.isToShow === true ? "visible" : "hidden") }}>
          <Login></Login>
        </div>
      )
    }
  }
  shouldComponentUpdate() {
    return true;
  }
  componentDidMount() {
    setTimeout(() => {

      this.setState({ isToShow: true })
    }, 500);


  }
}

export default App;
