import React, { Component } from 'react'
import { Input } from './Input.c';
import { updateUserInfo, getUserInfo } from './../data.service'
import { Dropdown } from 'reactjs-dropdown-component';

export class Row extends Component {
  constructor() {
    super();
    this.getInputs = this.getInputs.bind(this);
    this.getSelectedValue = this.getSelectedValue.bind(this)
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this)
    this.state = {}
  }
  handleCheckboxChange(e) {
    let current = this.isChecked(e.target.name)
    let o = {
      [this.props.tabType]: this.getTabData()
    }

    o[this.props.tabType][e.target.name] = !current
    let p = {
      [e.target.name]: !current
    }

    this.setState(p)
    updateUserInfo(o)
  }
  isChecked(e) {
    let v = this.getSelectedValue(e);
    let current = (v == true || v == "true") ? true : false
    return current
  }
  render() {
    this.props.components.map(x => {
      if (x.type == "select") {
        x.values.map(y => {
          y.id === this.getSelectedValue(x.name) ? y.selected = true : y.selected = false; return y
        })
      } else {

      }
    })

    return (
      <div className="row">

        {this.getInputs()}
      </div>
    )
  }

  componentWillMount() {
    this.props.components.map(x => {
      if (x.type == "select") {
        x.values.map(y => {
          if (y.id === this.getSelectedValue(x.name)) {
            y.selected = true
            x.selectedItem = y.title
          }
          else { y.selected = false }
          return y
        })
      }
      else {

      }
    })
  }
  getInputs() {
    let c = [];
    this.props.components.forEach((e, i) => {
      if (e.type == 'select') {
        c.push(
          <div key={"sel_" + i} className="col-md-6 col-sm-12 col-xs-12">
            <div style={{ textAlign: 'left' }} class="section-headline margin-top-5 margin-bottom-5">
              <h6 style={{ fontSize: '14px' }}>{e.placeholder}</h6>
            </div>
            <div className="input-with-icon-left">
              <Dropdown
                title={e.selectedItem || e.placeholder}
                list={e.values}
                resetThenSet={this.resetThenSet}
              />
            </div>
          </div>
        )
        Object.assign(this.state, { [e.name]: e.values })

      }
      else if (e.type == 'boolean') {

        c.push(
          <div key={Math.random()} className="col-md-6 col-sm-12 col-xs-12">


            <div className="switch-container">
              <label className="switch">
                <input onChange={this.handleCheckboxChange} name={e.name}
                  type="checkbox" checked={this.state[e.name] == true ? true : false} />
                <span className="switch-button"></span> {this.getPlaceHolder(e)}</label>
            </div>
          </div>
        )

      }
      else {
        c.push(
          <div key={"in_" + i} className="col-md-6 col-sm-12 col-xs-12">
            <div className="input-with-icon-left">
              <Input config={e} tabType={this.props.tabType} />
            </div>
          </div>
        )
      }
    });
    return c

  }
  getPlaceHolder(k) {
    if (k.placeholder) return k.placeholder
    k = k.name || ""
    return (k || "")
      .replace(/([a-z]+)*([A-Z][a-z])/g, "$1 $2").split(' ')
      .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
      .join(' ')
  }
  getSelectedValue(k) {
    return this.getTabData()[k]
  }
  getTabData() {
    return getUserInfo()[this.props.tabType] || {}
  }
  resetThenSet = (id, key) => {
    let temp = JSON.parse(JSON.stringify(this.state[key]));
    let selected = null;
    temp.map(item => {
      if (item.id === id) {
        item.selected = true
        selected = item.id
      }
      else {
        item.selected = false
      }
    });
    let o = {
      [this.props.tabType]: this.getTabData()
    }

    o[this.props.tabType][key] = selected

    this.setState(o)
    updateUserInfo(o)
  }
}
