import React from 'react'
import SocialLogin from 'react-social-login'

const GButton = ({ children, triggerLogin, ...props }) => (

    <button className='google-login ripple-effect' onClick={triggerLogin} {...props}>
        <i className="icon-brand-google"></i> Start with Google
    </button>
)

const FButton = ({ children, triggerLogin, ...props }) => (

    <button className='facebook-login ripple-effect' onClick={triggerLogin} {...props}>
        <i className="icon-brand-facebook-f"></i> Start with Facebook
    </button>
)

const GoogleButton = SocialLogin(GButton)
const FacebookButton = SocialLogin(FButton)

export { GoogleButton, FacebookButton }