import React, { Component } from 'react';
import { handleUpload, updateUserInfo } from './data.service'

export class FileUpload extends Component {

    constructor() {
        super();
        this.state = { selectedFile: null, loaded: 0, }

        this.handleSelectedFile = this.handleSelectedFile.bind(this)
        this.handleFileUpload = this.handleFileUpload.bind(this)
    }
    handleSelectedFile = event => {
        this.setState({
            selectedFile: event.target.files[0],
            loaded: 0,
        })
    }

    handleFileUpload = () => {
        handleUpload("https://resume.thepridecircle.com:8082/uploadFile", this.state.selectedFile, (fCB) => {
            this.setState({ loaded: 100 })
            console.log(fCB)
            updateUserInfo({ resumeFile: fCB })
            alert("Resume uploaded, you can submit your profile now")
        })
    }
    render() {
        return (

            <div className="row" style={{ marginTop: "20px" }}>
                <div className="col-md-10">
                    <div className="keywords-container">
                        <div className="keyword-input-container">
                            <input style={{ height: "48px", lineHeight: "22px", padding: "10px 10px" }} type="file" onChange={this.handleSelectedFile} className="keyword-input" placeholder="Add Keywords" />
                        </div>

                        <div className="clearfix"></div>
                    </div>
                </div>
                <div className="col-md-2">
                    <button
                        onClick={this.handleFileUpload}
                        className="button ripple-effect">
                        <i className="icon-feather-upload"></i>
                        Upload file
                            </button>

                </div>
            </div>
        )
    }
}