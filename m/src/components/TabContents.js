import React, { Component } from 'react'
import { TabComposer } from './builder/TabComposer.c';
import { FileUpload } from './FileUpload.c';

export class TabContents extends Component {
    render() {
        return (
            <div className="tabs-content" style={{ paddingBottom: '100px' }}>


                <div className={(this.props.activeTab === 1 ? 'tab active' : 'tab')} data-tab-id="1">
                    <TabComposer tabContent={this.props.config.personal} tabType="personal" />
                    <input type="submit" onClick={(e) => this.nextTab(2)} className="submit button margin-top-15" value="Save" style={{ marginLeft: "925px" }}></input>
                </div>

                <div className={(this.props.activeTab === 2 ? 'tab active' : 'tab')} data-tab-id="2">
                    <TabComposer tabContent={this.props.config.education} tabType="education" />
                    <input type="submit" onClick={(e) => this.nextTab(3)} className="submit button margin-top-15" value="Save" style={{ marginLeft: "925px" }}></input>
                </div>

                <div className={(this.props.activeTab === 3 ? 'tab active' : 'tab')} data-tab-id="3">
                    <TabComposer tabContent={this.props.config.employment} tabType="employment" />
                    <input type="submit" onClick={(e) => this.nextTab(4)} className="submit button margin-top-15" value="Save" style={{ marginLeft: "925px" }}></input>
                </div>
                <div className={(this.props.activeTab === 4 ? 'tab active' : 'tab')} data-tab-id="4">
                    <h3>Attach your resume</h3>
                    <FileUpload></FileUpload>
                    <input type="submit" onClick={(e) => this.nextTab(5)}
                        className="button ripple-effect big margin-top-30"
                        value="Submit your profile"
                        style={{ marginLeft: "800px" }}></input>
                </div>
            </div>
        )
    }
}